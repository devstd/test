﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// Decice class provide observable implementation
    /// </summary>
    abstract class DeviceBase : IDevice
    {
        private readonly IList<IObserver<IDevice>> _observers = new List<IObserver<IDevice>>();
        private readonly IDeviceDataHolder _dataHolder;



        /// <summary>
        /// ctor
        /// </summary>
        protected DeviceBase(IDeviceDataHolder dataHolder)
        {
            _dataHolder = dataHolder; 
        }

        /// <summary>
        /// Start device
        /// </summary>
        public virtual void StartDevice()
        {
            var t = new Thread(ProduceMeasurements);
            t.Start();
            t.Join();
        }

        /// <summary>
        /// Notify observers
        /// </summary>
        protected void NotifyObserver()
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(this);
            }
        }

        /// <summary>
        /// Abstract method to produce measurements. Should be implemented to concrete device
        /// </summary>
        protected abstract void ProduceMeasurements();

  
        /// <summary>
        /// Device data holder
        /// </summary>
        public IDeviceDataHolder DataHolder
        {
            get { return _dataHolder; }
        }

        /// <summary>
        /// Subscribe observer
        /// </summary>
        /// <param name="observer"></param>
        /// <returns></returns>
        public IDisposable Subscribe(IObserver<IDevice> observer)
        {
            if (!_observers.Contains(observer))
                _observers.Add(observer);
            return new Unsubscriber(_observers, observer);
        }

        /// <summary>
        /// Complete transmission
        /// </summary>
        public virtual void ShutDown()
        {
            foreach (var observer in _observers.ToArray())
                if (_observers.Contains(observer))
                    observer.OnCompleted();

            _observers.Clear();
        }

        /// <summary>
        /// Unsubscriber object enables observers to stop receiving notifications
        /// </summary>
        private class Unsubscriber : IDisposable
        {
            private readonly IList<IObserver<IDevice>> _observers;
            private readonly IObserver<IDevice> _observer;

            public Unsubscriber(IList<IObserver<IDevice>> observers, IObserver<IDevice> observer)
            {
                _observers = observers;
                _observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }
   }
}
