﻿using System;
using System.Threading;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// Random generator helper
    /// </summary>
    static class StaticRandom
    {
        private static int _seed;

        private static readonly ThreadLocal<Random> ThreadLocal = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref _seed)));

        /// <summary>
        /// ctor
        /// </summary>
        static StaticRandom()
        {
            _seed = Environment.TickCount;
        }

        /// <summary>
        /// Singelton instance
        /// </summary>
        public static Random Instance { get { return ThreadLocal.Value; } }
    }
}
