﻿using System;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// IDevice
    /// </summary>
    interface IDevice : IObservable<IDevice>
    {
        /// <summary>
        /// Data holder
        /// </summary>
        IDeviceDataHolder DataHolder { get; } 

        /// <summary>
        /// Shutting down
        /// </summary>
        void ShutDown();

        /// <summary>
        /// Start device
        /// </summary>
        void StartDevice();

    }
}
