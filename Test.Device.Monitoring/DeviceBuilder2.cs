﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// override fatory method to return a instance concreate device2
    /// </summary>
    class DeviceBuilder2 : DeviceBuilder
    {
        /// <summary>
        /// Construct ConcreteDevice2
        /// </summary>
        /// <returns></returns>
        public override IDevice Construct()
        {
            return new ConcreteDevice2();
        }
    }
}
