﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// Concreate Monitoring provides the IObserver<IDevice> implementation
    /// </summary>
    sealed class DeviceMonitoring : IDeviceMonitoring<IDevice>
    {
        private readonly string _name;
        private readonly Dictionary<string, List<int>> _deviceList;
        private IDisposable _unsubscriber;
        
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="name"></param>
        public DeviceMonitoring(string name)
        {
            _name = name;
            _deviceList = new Dictionary<string, List<int>>();
        }

        /// <summary>
        /// Notifies the observer that the provider has finished sending push-based notifications
        /// </summary>
        public void OnCompleted()
        {
       //     Console.WriteLine("The Device has completed transmitting data to {0}.", _name);
            Unsubscribe();
        }

        /// <summary>
        /// Notifies the observer that the provider has experienced an error condition.
        /// </summary>
        /// <param name="error"></param>
        public void OnError(Exception error)
        {
          // todo handle exception
        }

        /// <summary>
        /// Subscribe observer
        /// </summary>
        /// <param name="provider"></param>
        public void Subscribe(IObservable<IDevice> provider)
        {
            if (provider != null)
                _unsubscriber = provider.Subscribe(this);
        }

        /// <summary>
        /// Unsubscribe observer
        /// </summary>
        private void Unsubscribe()
        {
       //     Console.WriteLine("Observer: {0} Unsubscribe", _name);
            _unsubscriber.Dispose();
        }

        /// <summary>
        /// Display number of messages you got or read from the devices. 
        /// </summary>
        public void ShowResult()
        {
            foreach (var device in _deviceList)
                Console.WriteLine(DisplayResult(device.Key));
            // Console.WriteLine("Observer: {0} Device ID: {1} MessageReceived: {2}", _name, device.Key, device.Value);
        }

        /// <summary>
        /// Provides the observer with new data.
        /// </summary>
        /// <param name="value"></param>
        public void OnNext(IDevice value)
        {
            if (!_deviceList.ContainsKey(value.DataHolder.DeviceName))
                _deviceList.Add(value.DataHolder.DeviceName, new List<int>());

            var measurementList = _deviceList[value.DataHolder.DeviceName];
            measurementList.Add(value.DataHolder.Measurement); 

           // Console.WriteLine("Observer: {0} Device ID: {1} Measurement: {2} Message Count: {3}", _name, value.DataHolder.DeviceName, value.DataHolder.Measurement, value.DataHolder.MessageCount);
        }

        /// <summary>
        /// Display Measurement result
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        private string DisplayResult(String deviceId)
        {
          return String.Concat("Device ID: ", deviceId, " Measurement: ", string.Join(", ",
              _deviceList.Where(x => x.Key == deviceId).SelectMany(t => t.Value).ToArray()));
        }
    }
}
