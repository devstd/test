﻿using System;
using System.Collections.Generic;

namespace Test.Device.Monitoring
{

    /// <summary>
    ///Monitoring simulator
    /// </summary>
    sealed class MonitoringSimulator : IDisposable
    {
        private readonly IList<IDevice> _devices;
        private readonly IDeviceMonitoring<IDevice> _monitoring;
        private readonly int _deviceCount = 10;
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="monitoring"></param>
        public MonitoringSimulator(IDeviceMonitoring<IDevice> monitoring)
        {
            _devices = new List<IDevice>();
            _monitoring = monitoring;
       }

        /// <summary>
        /// Attach device
        /// </summary>
        /// <param name="count"></param>
        public void AttachDevices(IDevice device) 
        {
            _monitoring.Subscribe(device);
            _devices.Add(device);
        }

        /// <summary>
        /// Execute
        /// </summary>
        public void Execute()
        {
            foreach(var device in _devices)
                device.StartDevice();

            _monitoring.ShowResult();
        }


        /// <summary>
        /// fake release unmanaged resources
        /// </summary>
        public void Dispose()
        {
            foreach (var device in _devices)
                device.ShutDown();
        }

        /// <summary>
        /// Generate fake data
        /// </summary>
        internal void ConstructDevices()
        {
            List<DeviceBuilder> builders = new List<DeviceBuilder>();
            builders.Add(new DeviceBuilder1());
            builders.Add(new DeviceBuilder2());
            for (var i = 0; i < _deviceCount; i++)
            {
                var builderIndex = i % 2 == 0 ? 0 : 1;
                AttachDevices(builders[builderIndex].Construct());
            }
        }
    }
}
