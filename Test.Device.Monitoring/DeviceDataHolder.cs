﻿using System;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// Decice data holder
    /// </summary>
    class DeviceDataHolder : IDeviceDataHolder
    {
        private int _measurement;
        private readonly string _deviceName;

        /// <summary>
        /// ctor
        /// </summary>
        public DeviceDataHolder()
        {
            _deviceName = Guid.NewGuid().ToString("N").ToUpper();
        }

        /// <summary>
        /// Device name
        /// </summary>
        public string DeviceName { get { return _deviceName; } }


        /// <summary>
        /// Return Measurement
        /// </summary>
        public int Measurement
        {
            get { return _measurement; }
        }

        /// <summary>
        /// Set measurement
        /// </summary>
        /// <param name="value"></param>
        public void SetMeasurement(int value)
        {
            _measurement = value;
        }
    }
}
