﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;

namespace Test.Device.Monitoring
{
    public static class Programm
    {
        public static void Main()
        {
            // TODO here should be IoC  container for registering IDeviceMonitoring
            using (var simulator = new MonitoringSimulator(new DeviceMonitoring("Monitoring - 1")))
            {
                simulator.ConstructDevices();
                simulator.Execute();
            }
            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();
        }

    }
}
