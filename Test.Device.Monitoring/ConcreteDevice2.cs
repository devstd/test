﻿namespace Test.Device.Monitoring
{
    /// <summary>
    ///  Device2 class  implementation
    /// </summary>
    class ConcreteDevice2 : DeviceBase
    {
        public ConcreteDevice2()
            : base(new DeviceDataHolder())
        {
        }
        /// <summary>
        /// Produce measurement
        /// </summary>
        protected override void ProduceMeasurements()
        {
            for (var i = 0; i < StaticRandom.Instance.Next(1, 200); i++)
            {
                DataHolder.SetMeasurement(StaticRandom.Instance.Next(1, 300));
                NotifyObserver();
            }
        }
    }
}
