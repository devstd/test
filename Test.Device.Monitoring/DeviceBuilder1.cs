﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// override fatory method to return a instance concreate device1
    /// </summary>
    class DeviceBuilder1 : DeviceBuilder
    {
        /// <summary>
        /// Construct ConcreteDevice1
        /// </summary>
        /// <returns></returns>
        public override IDevice Construct()
        {
            return new ConcreteDevice1();
        }
    }
}
