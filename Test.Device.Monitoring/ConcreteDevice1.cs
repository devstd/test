﻿namespace Test.Device.Monitoring
{
    /// <summary>
    ///  Device1 class  implementation
    /// </summary>
    class ConcreteDevice1 : DeviceBase
    {
       public ConcreteDevice1()
            : base(new DeviceDataHolder())
        { 
        }

        /// <summary> 
        /// Produce measurement
        /// </summary>
        protected override void ProduceMeasurements()
        {
            for (var i = 0; i < StaticRandom.Instance.Next(1, 100); i++)
            {
                DataHolder.SetMeasurement(StaticRandom.Instance.Next(1, 400));
                NotifyObserver();
            }
        }
    }
}
