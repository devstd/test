﻿using System;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// Device monitoring interface
    /// </summary>
    /// <typeparam name="TDevice"></typeparam>
    interface IDeviceMonitoring<in TDevice> : IObserver<TDevice>
    {
        /// <summary>
        /// Show result
        /// </summary>
        void ShowResult();

        /// <summary>
        /// Subscribe observer
        /// </summary>
        /// <param name="provider"></param>
        void Subscribe(IObservable<TDevice> provider);
    }
}
