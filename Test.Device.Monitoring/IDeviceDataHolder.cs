﻿namespace Test.Device.Monitoring
{
    /// <summary>
    /// Device data holder
    /// </summary>
    interface IDeviceDataHolder
    {
        /// <summary>
        /// Device name
        /// </summary>
        string DeviceName { get; }


        /// <summary>
        /// Measurement
        /// </summary>
        int Measurement { get;  }

        /// <summary>
        /// Set measurement
        /// </summary>
        /// <param name="value"></param>
        void SetMeasurement(int value);
    } 
}
