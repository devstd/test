﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Device.Monitoring
{
    /// <summary>
    /// declares the factory method which returns an object of type IDevice
    /// </summary>
    abstract class DeviceBuilder
    {
        /// <summary>
        /// Construct device
        /// </summary>
        /// <returns></returns>
        public abstract IDevice Construct();
    }
}
