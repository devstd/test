# README #

### PROBLEM ###

* The problem to solve is following:
*                You monitor devices, which are sending data to you.
*                Each device have a unique name.
*                Each device produces measurements.
 
* Challange is:
*                Compute number of messages you got or read from the devices.
 

### How do I get set up? ###
1. Download source code
2. Open solution Test.Device.Monitoring.sln in Visual Studio 2010 or newer version and press F6 for rebuild poject
3. In build output directory run Test.Device.Monitoring.exe for monitoring devices


### Contribution guidelines ###

* Code review